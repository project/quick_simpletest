<?php
/**
 * @file
 * Sub test classes needed by the functionality test in the test file.
 *
 * The functionality test of Quick SimpleTest will start running sub tests that
 * extend QuickSimpletTestTestCase to study their behavior.
 * To prevent SimpleTest from finding them when creating a list of test, they
 * are outsourced into this file that gets included by the setUp method
 * of quick_simpletests functionality test.
 */

// Test class for testing assertions.
class QuickSimpleTestAssertionsTestCase extends QuickSimpleTestTestCase {

  /**
   * Expected to pass.
   */
  protected function testSucceed() {
    $this->AssertTrue(TRUE);
  }

  /**
   * Expected to fail.
   */
  protected function testFail() {
    $this->AssertTrue(FALSE);
  }

}

// Test class for testing database actions.
class QuickSimpleTestDatabaseTestCase extends QuickSimpleTestTestCase {

  /**
   * Create a database structure in the context of the Star Trek universe.
   */
  protected function setUp() {

    parent::setUp();

    // Create a table to store star ships/stations.
    db_create_table('ships', array(
      'description' => 'List of star ships and there properties.',
      'fields' => array(
        'ship_id' => array(
          'description' => 'The unique identifier for a ship.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'name' => array(
          'description' => 'The name of the ship.',
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE,
        ),
        'registry' => array(
          'description' => 'The registry number of the ship.',
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ship_id' => array('ship_id'),
        'registry' => array('registry'),
      ),
      'primary key' => array('ship_id'),
    ));

    // Create a table for storing persons.
    db_create_table('persons', array(
      'description' => 'Information on a person.',
      'fields' => array(
        'person_id' => array(
          'description' => 'The unique identifier of the person.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'name' => array(
          'description' => 'The name of the person.',
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE,
        ),
        'profession' => array(
          'description' => 'The profession of the person.',
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ),
      ),
      'primary key' => array('person_id'),
      'unique keys' => array(
        'person_id' => array('person_id'),
      ),
    ));

    // Create a table to store crews to the ships.
    db_create_table('crews', array(
    'description' => 'Crewmen on the ship.',
    'fields' => array(
      'ship_id' => array(
        'description' => 'The unique identifier of the matching ship.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'person_id' => array(
        'description' => 'The unique identifier of the person.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'function' => array(
        'description' => 'The function of the crewman on the ship. For example \'captain\'',
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
      ),
    ),
    'foreign keys' => array(
      'person' => array(
        'table' => 'persons',
        'columns' => array('person_id' => 'person_id'),
      ),
      'ship_id' => array(
        'table' => 'ships',
        'columns' => array('ship_id' => 'ship_id'),
      ),
    ),
    ));

    // Insert some test persons.
    db_insert('persons')
    ->fields(array('person_id', 'name', 'profession'))
    ->values(array(
      'person_id' => 0,
      'name' => 'Picard',
      'profession' => 'officer',
    ))
    ->values(array(
      'person_id' => 1,
      'name' => 'Worf',
      'profession' => 'tactical officer',
    ))
    ->values(array(
      'person_id' => 2,
      'name' => 'Sisko',
      'profession' => 'officer',
    ))
    ->values(array(
      'person_id' => 3,
      'name' => 'Bashir',
      'profession' => 'medical officer',
    ))
    ->execute();

    // Create some test ships/stations.
    db_insert('ships')
    ->fields(array('ship_id', 'name', 'registry'))
    ->values(array(
      'ship_id' => 0,
      'name' => 'Defiant',
      'registry' => 'NX-74250',
    ))
    ->values(array(
      'ship_id' => 1,
      'name' => 'Enterprise-D',
      'registry' => 'NCC-1701-D',
    ))
    ->values(array(
      'ship_id' => 2,
      'name' => 'Deep Space Nine',
      'registry' => 'DS9',
    ))
    ->execute();

    // Create crews.
    db_insert('crews')
    ->fields(array('person_id', 'ship_id', 'function'))
    // Add Picard as captain to enterprise-D.
    ->values(array(
      'person_id' => 0,
      'ship_id' => 1,
      'function' => 'captain',
    ))
    // Add Worf as chief of security to the enterprise-D.
    ->values(array(
      'person_id' => 1,
      'ship_id' => 1,
      'function' => 'security chief',
    ))
    // Add Worf as first officer to the defiant.
    ->values(array(
      'person_id' => 1,
      'ship_id' => 0,
      'function' => 'first officer',
    ))
    // Add Worf as strategic operations officer to DS9.
    ->values(array(
      'person_id' => 1,
      'ship_id' => 2,
      'function' => 'strategic operations officer',
    ))
    // Add Sisko as commander to DS9.
    ->values(array(
      'person_id' => 2,
      'ship_id' => 2,
      'function' => 'commander',
    ))
    // Add Sisko as captain to the defiant.
    ->values(array(
      'person_id' => 2,
      'ship_id' => 1,
      'function' => 'captain',
    ))
    // Add Bashir as chief medical officer to DS9.
    ->values(array(
      'person_id' => 3,
      'ship_id' => 2,
      'function' => 'chief medical officer',
    ))
    ->execute();
  }

  /**
   * Test that a joined select will give the expected result.
   */
  protected function testJoinedSelect() {
    // Try to get all ships Worf is working on.
    $query = db_select('ships')
    ->fields('ships', array('name'));
    $query->join('crews', NULL, 'ships.ship_id = crews.ship_id');
    $query->join('persons', NULL, 'crews.person_id = persons.person_id AND persons.name = \'Worf\'');
    $result = $query->execute();

    // Check the list of ships.
    $this->AssertEqual($result->fetchCol(),
    array('Enterprise-D', 'Defiant', 'Deep Space Nine'));
  }

  /**
   * Test that after an insert we can get the just inserted data.
   */
  protected function testInsert() {
    // Try to add Voyageur to the list of ships.
    db_insert('ships')
    ->fields(array(
      'ship_id' => 100,
      'name' => 'Voyageur',
      'registry' => 'NCC-74656',
     ))
    ->execute();

    // Check that insertion worked as expected.
    $row = db_select('ships', 's')
    ->fields('s')
    ->condition('ship_id', 100)
    ->execute()
    ->fetchAssoc();
    $this->AssertEqual($row, array(
      'ship_id' => 100,
      'name' => 'Voyageur',
      'registry' => 'NCC-74656',
    ));
  }

  /**
   * Test that we can create a table.
   */
  protected function testCreateTable() {

    // Create a table to store a list of rooms on ships.
    db_create_table('rooms', array(
      'fields' => array(
        'ship_id' => array(
          'description' => 'The unique id for the ship, this room is on.',
          'type' => 'int',
          'not null' => TRUE,
          'unsigned' => TRUE,
        ),
        'deck' => array(
          'description' => 'The deck this room is on.',
        ),
        'name' => array(
          'description' => 'The name of the room.',
          'type' => 'varchar',
          'length' => 64,
        ),
      ),
      'foreign keys' => array(
        'ship_id' => array(
          'table' => 'ships',
          'columns' => array('ship_id' => 'ship_id'),
        ),
      ),
    ));

    // Verify result.
    db_insert('rooms')
    ->fields(array('ship_id' => 1, 'deck' => 9, 'name' => 'Nine Forward'))
    ->execute();
    $result = db_select('rooms')
    ->fields('rooms')
    ->execute()
    ->fetchAssoc();
    $this->assertEqual($result, array(
      'ship_id' => 1,
      'deck' => 9,
      'name' => 'Nine Forward',
    ));
  }

  /**
   * Delete some data from the database and make sure it's really gone.
   */
  protected function testDelete() {
    // Remove Picard from list of people.
    db_delete('persons')
    ->condition('name', 'Picard', '=')
    ->execute();

    // Check that deletion worked as expected.
    $result = db_select('persons')
    ->condition('name', 'Picard', '=')
    ->fields('persons')
    ->execute()
    ->fetchField();
    $this->AssertFalse($result);
  }

}

// Test class for testing curl requests.
class QuickSimpleTestCurlTestCase extends QuickSimpleTestTestCase {

  /**
   * Create a basic page and look if we can see it inside a curl request.
   */
  protected function testCurlLandsInSandbox() {
    // Create a basic page.
    $node = $this->drupalCreateNode(array(
    'type' => 'page',
    'status' => NODE_PUBLISHED,
    'title' => 'test_node',
    ));

    // Get the HTML on the node call.
    $this->drupalGet('node/' . $node->nid);

    // Get the title from HTML.
    $title = $this->xpath('//title');

    $this->assertTrue(preg_match("!^$node->title.*$!", ((string) $title[0])));
  }
}

// Test class for testing cache creation and existence.
class QuickSimpleTestCacheTestCase extends QuickSimpleTestTestCase {

  // Path to the profile cache folder.
  protected $profileCacheFolder;

  // Path to the module cache folder..
  protected $moduleCacheFolder;

  // Hash of the profile cache folder.
  protected $profileCacheFolderHash;

  // Hash of the module cache folder.
  protected $moduleCacheFolderHash;

  // An id for the current test run.
  protected $testID;

  /**
   * Create a test with some modules to enable and store information on it.
   */
  protected function setUp() {

    // List of example modules to enable.
    $modules = array('book', 'taxonomy');

    // Condition will be true, if this is the first call to setUp().
    if (!isset($this->profileCacheFolder)) {

      // Initialize test id.
      $this->testID = 0;
    }

    // We will use the HTTP_REFERER to pass our own identifier to the..
    // watchdog table ( into referer column ) .
    // We can't use the databasePrefix here, because it will be altered
    // by setUp().
    $_SERVER['HTTP_REFERER'] = 'quick_simpletest' . $this->testID;


    // Before the first test gets run, we will have to set up an empty test,
    // so that the caches get created and that we can store some information
    // on them, as they were after the first setUp() call.
    if (!isset($this->profileCacheFolder)) {
      // Setup an empty test.
      call_user_func_array('parent::setUp', $modules);

      // Store path to the caches and create checksums on them.
      $this->profileCacheFolder = $this->originalFileDirectory . '/quick_simpletest/profiles/' . $this->profile;
      $this->moduleCacheFolder = $this->originalFileDirectory . '/quick_simpletest/test_classes/QuickSimpleTestCacheTestCase';
      $this->profileCacheFolderHash = quick_simpletest_sha1r($this->profileCacheFolder);
      $this->moduleCacheFolderHash = quick_simpletest_sha1r($this->moduleCacheFolder);

      // Tear down empty test.
      $this->tearDown();
    }

    // Increment test_id.
    $this->testID++;

    // Set up sandbox for current test run.
    call_user_func_array('parent::setUp', $modules);
  }

  /**
   * Check that the folder for profile cache gets created.
   */
  protected function testCreationProfileFolder() {

    $this->assertTrue(is_dir($this->profileCacheFolder));
  }

  /**
   * Check that the folder for module cache gets created.
   */
  protected function testCreationModuleCacheFolder() {

    $this->assertTrue(is_dir($this->moduleCacheFolder));
  }

  /**
   * Make sure that profile cache remains unaffected of changes in the sandbox.
   */
  protected function testProfileCacheRemainsUnchanged() {
    $this->createDummyStuff();

    // Check profile cache is still the same.
    $this->assertEqual($this->profileCacheFolderHash,
    quick_simpletest_sha1r($this->profileCacheFolder));
  }

  /**
   * Make sure that module cache remains unaffected of changes in the sandbox.
   */
  protected function testModuleCacheRemainsUnchanged() {
    $this->createDummyStuff();

    // Check module cache is still the same.
    $this->assertEqual($this->moduleCacheFolderHash,
    quick_simpletest_sha1r($this->moduleCacheFolder));
  }

  /**
   * Make sure that profile cache gets used by the test run.
   */
  protected function testProfileCacheGetsUsed() {

    // Make sure Drupal has been installed in the first test run.
    $this->assertEqual(quick_simpletest_get_module_referer($this->profile), 'quick_simpletest0');
  }

  /**
   * Make sure that module cache gets used by the test run.
   */
  protected function testModuleCacheGetsUsed() {

    // Make sure book module has been installed in the first test run.
    $this->assertEqual(quick_simpletest_get_module_referer('book'), 'quick_simpletest0');
  }

  /**
   * Create some database content and some content in the files folder.
   */
  protected function createDummyStuff() {
    // Make a change to the database.
    variable_set('quick_simpletest_test_values', 12343);

    // Add a file to the files directory.
    $file = fopen(variable_get('file_public_path', conf_path() . '/files') . '/test.txt', 'w');
    fwrite($file, 'test test');
    fclose($file);
  }

}

// Test class for testing how cache reacts on changes to the cache creation
// condition ( modules to enable, installation profile ).
class QuickSimpleTestCacheChangeConditionsTestCase extends QuickSimpleTestTestCase {

  // List of modules to enable.
  protected $modules;

  // An id for the current test run.
  protected $testID;

  /**
   * Create a test with some modules to enable and store information on it.
   */
  protected function setUp() {

    // Condition will be true, if this is the first call to setUp().
    if (!isset($this->testID)) {

      // Initialize test id.
      $this->testID = 0;
    }

    // We will use the HTTP_REFERER to pass our own identifier to the
    // watchdog table ( into referer column ) .
    // We can't use the databasePrefix here, because it will be altered
    // by setUp().
    $_SERVER['HTTP_REFERER'] = 'quick_simpletest' . $this->testID;


    // Before the first test gets run, we will have to set up an empty test,
    // so that the caches get created and that we can study the behavior to
    // changes made to the given list of modules and the installation profile.
    if ($this->testID == 0) {

      $this->modules = array('book');
      $this->profile = 'minimal';

      // Set up empty test run.
      call_user_func_array('parent::setUp', $this->modules);

      // Tear down empty test.
      $this->tearDown();
    }

    // Increment test_id.
    $this->testID++;

    // We will use the HTTP_REFERER to pass our own identifier to the
    // watchdog table ( into referer column ) .
    // We can't use the databasePrefix here, because it will be altered
    // by setUp().
    $_SERVER['HTTP_REFERER'] = 'quick_simpletest' . $this->testID;

    // Moved call of parent::setUp to the test functions
    // to allow them to alter the conditions of the sandbox.
    //
    // We will have to set this to TRUE to allow running tests
    // without setting up sandbox.
    $this->setup = TRUE;
  }

  /**
   * Alter list of modules to enable and make sure module cache gets recreated.
   */
  protected function testChangeListOfModules() {
    // Add taxonomy module to list of modules to enable.
    $this->modules[] = 'taxonomy';
    call_user_func_array('parent::setUp', $this->modules);

    // Check that the module cache used has been created in this test run.
    $this->assertEqual(quick_simpletest_get_module_referer('book'), ('quick_simpletest' . $this->testID));
  }

  /**
   * Change the installations profile and make sure profile cache gets changed.
   */
  protected function testChangeInstallationsProfileProfileCache() {
    // Change installation profile.
    $this->profile = ($this->profile == 'standard') ? 'minimal' : 'standard';
    call_user_func_array('parent::setUp', $this->modules);

    // Check that profile module is enabled.
    $this->assertTrue(preg_match('!^quick_simpletest\d*$!',
    quick_simpletest_get_module_referer($this->profile)));
  }

  /**
   * Change the installations profile and make sure module cache gets changed.
   */
  protected function testChangeInstallationsProfileModuleCache() {
    // Change installation profile.
    $this->profile = ($this->profile == 'standard') ? 'minimal' : 'standard';
    call_user_func_array('parent::setUp', $this->modules);

    // Check that the module cache used has been created in this test run.
    $this->assertEqual(quick_simpletest_get_module_referer('book'), ('quick_simpletest' . $this->testID));

  }
}

// Test class for testing the do not cache modules option, that can
// be activated on the settings page.
class QuickSimpleTestDoNotCacheModulesOptionTestCase extends QuickSimpleTestTestCase {

  // An id for the current test run.
  protected $testID;

  /**
   * Disable module caching and create a test with some modules.
   */
  protected function setUp() {

    // Disable module caching.
    variable_set('quick_simpletest_cache_modules', FALSE);

    // Condition will be true, if this is the first call to setUp().
    if (!isset($this->testID)) {

      // Initialize test id.
      $this->testID = 0;
    }

    // We will use the HTTP_REFERER to pass our own identifier to the
    // watchdog table ( into referer column ) .
    // We can't use the databasePrefix here, because it will be altered
    // by setUp().
    $_SERVER['HTTP_REFERER'] = 'quick_simpletest' . $this->testID;

    // Before the first test gets run, we will have to set up an empty test,
    // so that the caches get created and that we can check if the modules gets
    // reinstalled.
    if ($this->testID == 0) {

      // Set up empty test run.
      parent::setUp('book');

      // Tear down empty test run.
      $this->tearDown();
    }

    // Setup sandbox.
    parent::setUp('book');
  }

  /**
   * Check that the do not cache modules option on the setting page works.
   *
   * The book module should be installed in this test run.
   */
  protected function testDoNotCacheModulesOptionsIsWorking() {

    $this->assertEqual(quick_simpletest_get_module_referer('book'), ('quick_simpletest' . $this->testID));
  }

}

/**
 * Gets the referer string matching the given module installation event.
 *
 * @param[in] module
 *   A module that has been enabled in the testrun.
 *
 * We use the HTTPD_REFERER to get the test id into the watchdog table.
 * This has to been done in the setUp call.
 * Then one can get the referer of when the module was installed. To
 * know in which test run the module cache has been created.
 */
function quick_simpletest_get_module_referer($module) {

  return db_select('watchdog', 'w')
  ->fields('w', array('referer'))
  ->condition('w.message', '%module module installed.')
  ->condition('w.variables', serialize(array('%module' => $module)))
  ->execute()
  ->fetchField();
}
