<?php

/**
 * @file
 * Provides QuickSimpleTestTestCase a faster version of DrupalWebTestCase.
 *
 * If you want to run a QuickSimpleTestCase test, you will have to extend the
 * class in this file.
 */

class QuickSimpleTestTestCase extends DrupalWebTestCase {

  /**
   * Creates profile files and database, if it hadn't been done yet.
   *
   * @return string
   *   path to the profile folder.
   *
   * If profile hasn't been created yet it will be created.
   *
   * Because this function might switch the databases,
   * it should be called in a batch operation.
   */
  protected function prepareProfileDirectory() {

    // Mask any evil characters in $profile.
    $profile = preg_replace('/[^[a-zA-Z0-9._-]]/', '#', $this->profile);

    // Create path to the profile folder.
    $profile_folder = $this->originalFileDirectory . '/quick_simpletest/profiles/' . $profile;

    // If profile hasn't been cached yet, cache it.
    if (!file_exists($profile_folder)) {

      // Create directory.
      if (!file_prepare_directory($profile_folder, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        // If directory can not be created, throw exception.
        throw new Exception(t('Can not create or mark executable directory "@directory".',
        array('@directory' => $profile_folder)));
      }

      // Create profile database.
      $database_key = 'quick_simpletest_profile_' . $profile;
      quick_simpletest_add_database($database_key, ($profile_folder . '/database'));

      // Install Drupal.
      $this->installDrupal($this->profile, $database_key, $profile_folder);

      // Remove database connection.
      Database::closeConnection('default', $database_key);
      Database::removeConnection($database_key);
    }

    return $profile_folder;
  }

  /**
   * Installs Drupal into the given database and into the given directory.
   *
   * @param[in] $database_key
   *   key to the database.
   *
   * @param[in] $directory
   *   folder in which to place the files that are created doing installation.
   *
   * @param[in] $profile
   *   the profile that is supposed to be installed.
   *
   * Because this function switches the databases,
   * it should be called in a batch operation
   */
  protected function installDrupal($profile, $database_key, $directory) {
    global $conf;
    // Setup.
    // Switch database.
    Database :: renameConnection('default', 'simpletest_original_default');
    Database :: renameConnection($database_key, 'default');

    // Create template test directory ahead of installation so fatal errors and
    // debug information can be logged during installation process.
    // Use temporary files directory with the same prefix as the database.
    $private_files_directory = $directory . '/private';
    $temp_files_directory = $private_files_directory . '/temp';

    // Create the directories.
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    file_prepare_directory($private_files_directory, FILE_CREATE_DIRECTORY);
    file_prepare_directory($temp_files_directory, FILE_CREATE_DIRECTORY);

    // Log fatal errors.
    ini_set('log_errors', 1);
    ini_set('error_log', $directory . '/error.log');

    // Preset the 'install_profile' system variable, so the first call into
    // system_rebuild_module_data()(in drupal_install_system()) will register
    // the profile as a module. Without this, the installation profile of
    // the parent site(executing the test) is registered, and the test
    // profile's hook_install() and other hook implementations are never
    // invoked.
    $conf['install_profile'] = $profile;

    include_once DRUPAL_ROOT . '/includes/install.inc';
    drupal_install_system();

    // Call DrupalWebTestCase::preloadRegistry.
    $this->preloadRegistry();

    // Set path variables.
    variable_set('file_public_path', $directory);
    variable_set('file_private_path', $private_files_directory);
    variable_set('file_temporary_path', $temp_files_directory);

    // Include the profile.
    variable_set('install_profile', $profile);
    $profile_details = install_profile_info($profile, 'en');

    // Install the modules specified by the testing profile.
    module_enable($profile_details['dependencies'], FALSE);

    // Run the profile tasks.
    $install_profile_module_exists = db_query("SELECT 1 FROM {system} WHERE type = 'module' AND name = :name", array(':name' => $profile))->fetchField();
    if ($install_profile_module_exists) {
      module_enable(array($profile), FALSE);
    }

    // Reset to old database.
    Database :: renameConnection('default', $database_key);
    Database :: renameConnection('simpletest_original_default', 'default');

  }

  /**
   * Prepares cache and tells you were to find the cached files.
   *
   * @param[in] $modules string
   *   list of modules that need to be installed and activated for the test run.
   *
   * @return string
   *   path to the cached Drupal profile and database.
   *
   * This function will install Drupal and the requested modules,
   * if this is not cached yet.
   * Also it will check if the cache is still valid and will refresh it, if
   * needed.
   *
   * Because this function might switch the databases,
   * it should be called in a batch operation.
   */
  protected function prepareSetupCache($modules) {

    // Get cached profile or create profile and cache it.
    $profile_template_folder = $this->prepareProfileDirectory($this->profile);

    // If there are no modules to cache return profile folder.
    if (!isset($modules) || empty($modules)) {
      return $profile_template_folder;
    }

    // This could have been passed in as either a single array argument
    // or a variable number of string arguments.
    // @todo Remove this compatibility layer in Drupal 8, and only accept
    // $modules as a single array.
    if (isset($modules[0]) && is_array($modules[0])) {
      $modules = $modules[0];
    }

    // Get test_class_folder.
    $test_class_folder = $this->originalFileDirectory . '/quick_simpletest/test_classes/' . get_class($this);

    // Check if the test class is already cached.
    $create_test_class = TRUE;

    // Check if cache entry exists and is still valid.
    $result = db_select('quick_simpletest_cache_info', 't')
    ->fields('t', array('cache_id', 'profile'))
    ->condition('t.test_class', get_class($this))
    ->execute()
    ->fetchAssoc();

    // Check directory for module cache exists.
    if (file_exists($test_class_folder)) {

      // If select query was successfully.
      if ($result) {

        // Make sure profile is still the same.
        if ($result['profile'] == $this->profile) {

          // Make sure requested modules are still the same.
          $db_modules = db_select('quick_simpletest_cache_info_modules', 't')
          ->fields('t', array('module'))
          ->condition('t.cache_id', $result['cache_id'])
          ->execute()
          ->fetchCol();

          sort($modules);
          sort($db_modules);
          if ($modules === $db_modules) {
            // We can skip installation of modules, if all checks went well.
            $create_test_class = FALSE;
          }
        }

      }

      // Remove test_class folder if it exists and test_class is invalid.
      if ($create_test_class) {
        file_unmanaged_delete_recursive($test_class_folder);
      }

    }

    // Delete database entry if it exists and is invalid.
    if ($create_test_class && $result) {

      db_delete('quick_simpletest_cache_info')
      ->condition('test_class', get_class($this))
      ->execute();

      db_delete('quick_simpletest_cache_info_modules')
      ->condition('cache_id', $result['cache_id'])
      ->execute();

    }

    if ($create_test_class) {

      quick_simpletest_copyr($profile_template_folder, $test_class_folder);

      // Add database for testing cache.
      quick_simpletest_add_database('test_class_database', $test_class_folder . '/database');

      // Install modules in database.
      $this->installModules($modules, 'test_class_database', $test_class_folder);

      // Remove database connection.
      Database::closeConnection('default', 'test_class_database');
      Database::removeConnection('test_class_database');

      // Add test_class information into database.
      $cache_id = db_insert('quick_simpletest_cache_info')
      ->fields(array(
        'test_class' => get_class($this),
        'profile' => $this->profile,
      ))
      ->execute();
      // Add list of modules.
      foreach ($modules as $module) {
        db_insert('quick_simpletest_cache_info_modules')
        ->fields(array(
          'cache_id' => $cache_id,
          'module' => $module,
        ))
        ->execute();
      }
    }

    return $test_class_folder;
  }

  /**
   * Install modules in the given directory and database.
   */
  protected function installModules($modules, $database_key, $directory) {

    // Switch database.
    Database :: renameConnection('default', 'simpletest_original_default');
    Database :: renameConnection($database_key, 'default');

    // Clear out information on modules from the static cache.
    drupal_static_reset('system_rebuild_module_data');

    // Set path variables.
    $private_files_directory = $directory . '/private';
    $temp_files_directory = $private_files_directory . '/temp';
    variable_set('file_public_path', $directory);
    variable_set('file_private_path', $private_files_directory);
    variable_set('file_temporary_path', $temp_files_directory);

    // Install modules needed for this test.
    if ($modules) {
      $success = module_enable($modules, TRUE);
      $this->assertTrue($success, t('Enabled modules: %modules', array('%modules' => implode(', ', $modules))));
    }

    // Reset cached schema for new database prefix. This must be done before
    // Drupal_flush_all_caches() so rebuilds can make use of the schema of
    // modules enabled on the cURL side.
    drupal_get_schema(NULL, TRUE);

    // Flush remaining caches.
    drupal_flush_all_caches();

    // Run cron once in that environment, as install.php does at the end of
    // the installation process.
    drupal_cron_run();

    // Reset to original database.
    Database :: renameConnection('default', $database_key);
    Database :: renameConnection('simpletest_original_default', 'default');

  }

  /**
   * This prepares the environment for each test run.
   */
  protected function setUp() {
    global $conf, $user, $language;

    // Set databasePrefix, will only be used as an internal identifier for
    // the current test run, since the quick_simpletest module doesn't work with
    // prefixed database connections. Instead it uses SQLite databases.
    $this->databasePrefix = 'simpletest' . mt_rand(1000, 1000000);

    // Store necessary current values.
    $this->originalLanguage = $language;
    $this->originalLanguageDefault = variable_get('language_default');
    $this->originalFileDirectory = variable_get('file_public_path', conf_path() . '/files');
    $this->originalProfile = drupal_get_profile();
    $clean_url_original = variable_get('clean_url', 0);

    // Set the test information for use in other parts of Drupal.
    $test_info = &$GLOBALS['drupal_test_info'];
    $test_info['test_run_id'] = $this->databasePrefix;
    $test_info['in_child_site'] = FALSE;

    // Save and clean shutdown callbacks array because it static cached and
    // will be changed by the test run. If we don't, then it will contain
    // callbacks from both environments. So testing environment will try
    // to call handlers from original environment.
    $callbacks = &drupal_register_shutdown_function();
    $this->originalShutdownCallbacks = $callbacks;
    $callbacks = array();

    // Set to English to prevent exceptions from utf8_truncate() from t()
    // during install if the current language is not 'en'.
    // The following array/object conversion is copied from language_default().
    $language = (object) array(
      'language' => 'en',
      'name' => 'English',
      'native' => 'English',
      'direction' => 0,
      'enabled' => 1,
      'plurals' => 0,
      'formula' => '',
      'domain' => '',
      'prefix' => '',
      'weight' => 0,
      'javascript' => '',
    );

    $modules = func_get_args();

    // Get cached setUp.
    //
    // Determine if caching modules is enabled.
    $cache_modules = variable_get('quick_simpletest_cache_modules', TRUE);

    // Reset all statics and variables to clean the environment.
    $conf = array();
    drupal_static_reset();

    if ($cache_modules) {
      $cache_directory = $this->prepareSetupCache($modules);
    }
    else {
      $cache_directory = $this->prepareProfileDirectory();
    }

    // Create test directory and copy files from cache into it.
    $public_files_directory = $this->originalFileDirectory . '/simpletest/' . drupal_substr($this->databasePrefix, 10);
    file_prepare_directory($public_files_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $conf['file_public_path'] = $public_files_directory;
    $private_files_directory = $public_files_directory . '/private';
    $temp_files_directory = $private_files_directory . '/temp';

    quick_simpletest_copyr($cache_directory, $public_files_directory);

    // Add database for the test run.
    quick_simpletest_add_database($this->databasePrefix, $public_files_directory . '/database');

    // Speed up the SQLite database. @see http://www.sqlite.org/pragma.html
    // for detail information on these options. We will have to do this with
    // db_query, because this is SQLite specific and there is now function for
    // PRAGMA in the database API.
    $query = 'PRAGMA synchronous = OFF;
    PRAGMA fullfsync = FALSE;
    PRAGMA journal_mode = MEMORY;
    PRAGMA temp_store = MEMORY;
    PRAGMA page_size = 4096;
    PRAGMA cache_size = 262144;';
    // Execute the query.
    Database::getConnection('default', $this->databasePrefix)
    ->query($query);

    // Install modules if we don't cache them.
    if (!$cache_modules) {
      $this->installModules($modules, $this->databasePrefix, $public_files_directory);
    }

    // Switch database.
    Database::renameConnection('default', 'simpletest_original_default');
    Database::renameConnection($this->databasePrefix, 'default');

    // Log fatal errors.
    ini_set('log_errors', 1);
    ini_set('error_log', $public_files_directory . '/error.log');

    // Set path variables.
    variable_set('file_public_path', $public_files_directory);
    variable_set('file_private_path', $private_files_directory);
    variable_set('file_temporary_path', $temp_files_directory);

    // Reset all static variables.
    drupal_static_reset();
    // Reset the list of enabled modules.
    module_list(TRUE);

    // Reload module files.
    module_load_all(FALSE);

    // Reload global $conf array and permissions.
    $this->refreshVariables();
    $this->checkPermissions(array(), TRUE);

    // Log in with a clean $user.
    $this->originalUser = $user;
    drupal_save_session(FALSE);
    $user = user_load(1);

    // Restore necessary variables.
    variable_set('install_task', 'done');
    variable_set('clean_url', $clean_url_original);
    variable_set('site_mail', 'simpletest@example.com');
    variable_set('date_default_timezone', date_default_timezone_get());
    // Set up English language.
    unset($GLOBALS['conf']['language_default']);
    $language = language_default();

    // Use the test mail class instead of the default mail handler class.
    variable_set('mail_system', array('default-system' => 'TestingMailSystem'));

    drupal_set_time_limit($this->timeLimit);
    $this->setup = TRUE;

  }

  /**
   * Delete created files and database from setUp().
   *
   * Only change to DrupalWebTestCase::tearDown is that this function
   * doesn't try to delete prefixed tables. This change is necessary,
   * because the Quick Simpletest module works with extra SQLite
   * databases, rather than with prefixed databases.
   */
  protected function tearDown() {
    global $user, $language;

    // In case a fatal error occurred that was not in the test process read the
    // log to pick up any fatal errors.
    simpletest_log_read($this->testId, $this->databasePrefix, get_class($this), TRUE);

    $emailCount = count(variable_get('drupal_test_email_collector', array()));
    if ($emailCount) {
      $message = format_plural($emailCount, '1 e-mail was sent during this test.', '@count e-mails were sent during this test.');
      $this->pass($message, t('E-mail'));
    }

    // Delete temporary files directory.
    file_unmanaged_delete_recursive($this->originalFileDirectory . '/simpletest/' . drupal_substr($this->databasePrefix, 10));

    // Get back to the original connection.
    Database::closeConnection('default', 'default');
    Database::removeConnection('default');
    Database::renameConnection('simpletest_original_default', 'default');

    // Restore original shutdown callbacks array to prevent original
    // environment of calling handlers from test run.c
    $callbacks = &drupal_register_shutdown_function();
    $callbacks = $this->originalShutdownCallbacks;

    // Return the user to the original one.
    $user = $this->originalUser;
    drupal_save_session(TRUE);

    // Ensure that internal logged in variable and cURL options are reset.
    $this->loggedInUser = FALSE;
    $this->additionalCurlOptions = array();

    // Reload module list and implementations to ensure that test module hooks
    // aren't called after tests.
    module_list(TRUE);
    module_implements('', FALSE, TRUE);

    // Reset the Field API.
    field_cache_clear();

    // Rebuild caches.
    $this->refreshVariables();

    // Reset language.
    $language = $this->originalLanguage;
    if ($this->originalLanguageDefault) {
      $GLOBALS['conf']['language_default'] = $this->originalLanguageDefault;
    }

    // Close the CURL handler.
    $this->curlClose();
  }

  /**
   * Initializes the cURL connection.
   *
   * If the simpletest_httpauth_credentials variable is set, this function will
   * add HTTP authentication headers. This is necessary for testing sites that
   * are protected by login credentials from public access.
   * See the description of $curl_options for other options.
   *
   * Only change to DrupalWebTestCase::curlInitialize() is, that this
   * function doesn't includes a database prefix in the user agent string
   * of curl, because the Quick SimpleTest module works with extra SQLite
   * databases, rather than with prefixed databases.
   */
  protected function curlInitialize() {
    global $base_url;

    if (!isset($this->curlHandle)) {
      $this->curlHandle = curl_init();

      // Included bugfix to prevent that curl options don't get set
      // when CURLOPT_COOKIEJAR is set to NULL in some PHP versions.
      // @see http://drupal.org/node/1671200
      // @todo replace this bugfix with official bugfix from SimpleTest
      // when it gets implemented.
      if (empty($this->cookieFile)) {
        $this->cookieFile = tempnam(NULL, "drupal-simpletest-cookiejar-");
      }

      $curl_options = array(
        CURLOPT_COOKIEJAR => $this->cookieFile,
        CURLOPT_URL => $base_url,
        CURLOPT_FOLLOWLOCATION => FALSE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE, // Required to make the tests run on https.
        CURLOPT_SSL_VERIFYHOST => FALSE, // Required to make the tests run on https.
        CURLOPT_HEADERFUNCTION => array(&$this, 'curlHeaderCallback'),
        CURLOPT_USERAGENT => $this->databasePrefix,
      );
      if (isset($this->httpauth_credentials)) {
        $curl_options[CURLOPT_HTTPAUTH] = $this->httpauth_method;
        $curl_options[CURLOPT_USERPWD] = $this->httpauth_credentials;
      }
      // curl_setopt_array() returns FALSE if any of the specified options
      // cannot be set, and stops processing any further options.
      $result = curl_setopt_array($this->curlHandle, $this->additionalCurlOptions + $curl_options);
      if (!$result) {
        throw new \UnexpectedValueException('One or more cURL options could not be set.');
      }

      // By default, the child session name should be the same as the parent.
      $this->session_name = session_name();
    }
    // We set the user agent header on each request so as to use the current
    // time and a new uniqid.
    if (preg_match('/simpletest\d+/', $this->databasePrefix, $matches)) {

      $connection_info = Database::getConnectionInfo('default', 'default');
      $path = $connection_info['default']['database'];
      // Replace "&" and ";" in the path, to prevent it from breaking the
      // user agent string.
      $path = preg_replace(array('!&!', '!;!'), array('&amp;', '&#59;'), $path);

      curl_setopt($this->curlHandle, CURLOPT_USERAGENT, drupal_generate_test_ua("$matches[0]:quick_simpletest:$path"));
    }
  }

}
