CONTENTS OF THIS FILE 
---------------------

* About 
* Installation
* Usage
* Removal

ABOUT
-----

This module extends the SimpleTest module to allow module developers to run a
faster and fully compatible version of it's DrupalWebTestCase test, with a
information loss kept near the minimum. It has been created to make test driven
development less painful, without having to switch to a different testing
framework than the standard one.

To achieve a major speed improvement Quick SimpleTest caches parts of the
setUp() method of the test run, which gets called before every test method.
This takes a very long time to finish up, because it will install a hole Drupal
environment and then enable a list of requested or required modules. This module
will make save marks after these two operations and on the next call to setUp it
will start from them.

INSTALLATION
------------

You will have to patch your bootstrap.inc in includes with the
quick_simpletest_bootstrap.patch coming with this module. Please read
INSTALL.txt which gives you detailed information on how to do so.

USAGE
-----

Let your tests extend QuickSimpleTestTestCase instead of DrupalWebTestCase. You
can change back and forth as often you want to. To clear out certain/all caches
you can visit the settings page under admin/config/development/testing/settings.
There you can also enable/disable the caching of module enabling.

REMOVAL
-------

If you want to uninstall this module, please read the removal instructions in
INSTALL.txt.
